let mongoose = require('mongoose');
const { check } = require('express-validator/check');

var RoleSchema = mongoose.Schema({
	id: String,
	grants: [String]
});

RoleSchema.statics.search = function(query, callback) {
	return this.find({
		$or: [
			{
				'id': new RegExp(query, 'i')
			},
			{
				'grants': { $in: [new RegExp(query, 'i')] }
			}
		]
	}).exec(callback);
};

RoleSchema.statics.validations = [
	check('id').exists('The role ID is required.'),
	check('grants').exists().withMessage('At least one grant is required.'),
];

module.exports = mongoose.model('Role', RoleSchema);
