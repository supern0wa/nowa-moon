var faker = require('faker');

module.exports = (amount, data) => {
	var batch = [];

	for (var i = 0; i < (amount || 1); i++) {
		batch.push(Object.assign({
			id: faker.hacker.noun(),
			grants: faker.random.words(5).split(' ')
		}, data || {}));
	}

	return batch;
};
