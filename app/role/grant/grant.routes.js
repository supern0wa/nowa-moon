module.exports = (router, request) => {
	var Controller = require('./grant.controller')(request);

	router.post('/roles/:id/grants', Controller.set)
	router.patch('/roles/:id/grants', Controller.add)
	router.delete('/roles/:id/grants', Controller.remove)
}
