var mongoose = require('mongoose'),
	server = require('../../../server'),
	chai = require('chai'),
	assert = chai.assert;

var Role = require('../role.model');

chai.use(require('chai-http'));

describe('Grants', () => {
	// Remove all roles
	afterEach(done => {
		Role.remove({}, done)
	});

	describe('Endpoints', () => {
		it('Replaces role grants', done => {
			Role.create(require('../role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					// POST /roles/:id/grants
					chai.request(server)
						.post(`/roles/${roles[0].id}/grants`)
						.send({
							grants: ['write:event']
						})
						.end((err, res) => {
							if (err) return done(err);

							// Check if role grants was replaced
							assert.sameMembers(res.body.grants, ['write:event'])
							done();
						})
				}
			);
		});

		it('Adds grant to a existing role', done => {
			Role.create(require('../role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					// PATCH /roles/:id/grants
					chai.request(server)
						.patch(`/roles/${roles[0].id}/grants`)
						.send({
							grants: ['write:event']
						})
						.end((err, res) => {
							if (err) return done(err);

							// Check if grant was added to role
							assert.includeMembers(res.body.grants, ['write:event'])
							done();
						})
				}
			);
		});

		it('Removes multple grants from a role', done => {
			Role.create(require('../role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					// DELETE /roles/:id/grants
					chai.request(server)
						.delete(`/roles/${roles[0].id}/grants`)
						.send({
							grants: [
								roles[0].grants[0],
								roles[0].grants[1],
							]
						})
						.end((err, res) => {
							if (err) return done(err);

							// Check if grant was added to role
							assert.notIncludeMembers(res.body.grants, [
								roles[0].grants[0],
								roles[0].grants[1]
							]);
							done();
						})
				}
			);
		});
	});
});
