var Role = require('../role.model');

module.exports = (request) => ({
	set(req, res, next) {
		Role.findOneAndUpdate(req.params.query, {
			grants: req.body.grants
		}, { new: true }, (err, role) => {
			if (err) return next(err);

			res.json(role);
		});
	},

	add(req, res, next) {
		Role.findOneAndUpdate({ id: req.params.id }, {
			$push: {
				grants: req.body.grants
			}
		}, { new: true }, (err, role) => {
			if (err) return next(err);

			res.json(role);
		});
	},

	remove(req, res, next) {
		Role.findOneAndUpdate({ id: req.params.id }, {
			$pull: {
				grants: { $in: req.body.grants }
			}
		}, { new: true }, (err, role) => {
			if (err) return next(err);

			res.json(role);
		});
	}
})
