module.exports = (router, request) => {
	var Model = require('./role.model'),
		Controller = require('./role.controller')(request);

	router.get('/roles', Controller.all);
	router.get('/roles/:id', Controller.find);
	router.get('/roles/search/:query', Controller.search);
	router.post('/roles', Model.validations, Controller.store);
	router.patch('/roles/:id', Controller.update);
	router.delete('/roles/:id', Controller.destroy);

	// Grants
	require('./grant/grant.routes')(router, request);
}

