var Role = require('./role.model');

module.exports = (request) => ({
	all(req, res, next) {
		Role.find({}, (err, roles) => {
			if (err) return next(err);

			res.json(roles);
		});
	},

	find(req, res, next) {
		Role.findOne({ id: req.params.id }, (err, role) => {
			if (err) return next(err);

			res.json(role);
		});
	},

	search(req, res, next) {
		Role.search(req.params.query, (err, roles) => {
			if (err) return next(err);

			res.json(roles);
		});
	},

	store(req, res, next) {
		Role.create(req.body, (err, role) => {
			if (err) return next(err);

			res.json(role);
		})
	},

	update(req, res, next) {
		Role.findOneAndUpdate({ id: req.params.id }, req.body, { new: true }, (err, role) => {
			if (err) return next(err);

			res.json(role);
		});
	},

	destroy(req, res, next) {
		Role.remove({ id: req.params.id }, (err, role) => {
			if (err) return next(err);

			res.json(role)
		});
	},
})
