var mongoose = require('mongoose'),
	server = require('../../server'),
	chai = require('chai'),
	assert = chai.assert;

var Role = require('./role.model');

chai.use(require('chai-http'));

describe('Role', () => {
	// Remove all roles
	afterEach(done => {
		Role.remove({}, done)
	});

	describe('Endpoints', () => {
		it('Gets all role records', done => {
			Role.create(require('./role.factory')(10),
				(err, roles) => {
					if (err) return done(err);

					// GET request to /roles
					chai.request(server)
					.get('/roles')
					.end((err, res) => {
						if (err) return done(err);

						// Check if returned all the roles
						assert.equal(res.body.length, 10);

						done();
					});
				}
			);
		});

		it('Finds role by ID', done => {
			Role.create(require('./role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					// GET request to /roles
					chai.request(server)
					.get(`/roles/${roles[0].id}`)
					.end((err, res) => {
						if (err) return done(err);

						// Compare role IDs
						assert.equal(res.body.id, roles[0].id);

						// Compare role grants
						assert.sameMembers(res.body.grants, roles[0].grants);

						done();
					});
				}
			);
		});

		it('Searches for roles', done => {
			Role.create(require('./role.factory')(10),
				(err, roles) => {
					if (err) return done(err);

					// GET request to /roles/search/:query
					chai.request(server)
					.get(`/roles/search/${roles[0].grants[0]}`)
					.end((err, res) => {
						if (err) return done(err);

						assert.isOk(res.body.length > 0)
						done();
					});
				}
			);
		});

		it('Creates a new role', done => {
			chai.request(server)
			.post('/roles')
			.send({
				id: 'editor',
				grants: ['read:post', 'write:post']
			})
			.end((err, res) => {
				if (err) return done(err);

				// Verify role ID
				assert.equal(res.body.id, 'editor');

				// Check role grants
				assert.sameMembers(res.body.grants, ['read:post', 'write:post']);
				done();
			});
		});

		it('Updates the role object', done => {
			Role.create(require('./role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					chai.request(server)
					.patch(`/roles/${roles[0].id}`)
					.send({
						id: 'editor',
						grants: ['write:page']
					})
					.end((err, res) => {
						if (err) return done(err);

						// Verify role ID
						assert.equal(res.body.id, 'editor');

						// Check role grants
						assert.sameMembers(res.body.grants, ['write:page']);
						done();
					});
				}
			);
		});

		it('Deletes role from database', done => {
			Role.create(require('./role.factory')(1),
				(err, roles) => {
					if (err) return done(err);

					chai.request(server)
					.delete(`/roles/${roles[0].id}`)
					.end((err, res) => {
						if (err) return done(err);

						Role.findOne({ id: roles[0].id }, (err, role) => {
							if (err) return done(err);

							// Verify is role was deleted
							assert.isOk(!role);
							done();
						});
					})
				}
			);
		});
	});
});
