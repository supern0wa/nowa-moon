var mongoose = require('mongoose'),
	server = require('../../server'),
	chai = require('chai'),
	assert = chai.assert,
	database = require('../../config/database');

var User = require('./user.model');
var Role = require('../role/role.model');

chai.use(require('chai-http'));

describe('User', () => {
	// Remove all users and roles
	afterEach(done => {
		Role.remove({}, err =>
			User.remove({}, done)
		)
	});

	describe('Model', () => {
		// Connect to database
		before(done => {
			mongoose.connect(database.url, { useMongoClient: true }, done)
		});

		it('Validates password', done => {
			User.create(require('./user.factory')(1, {
				accounts: {
					local: {
						username: 'giseudo',
						password: '123123'
					}
				}
			}),
				(err, users) => {
					if (err) return done(err);

					// If password is valid
					assert.isOk(users[0].validPassword('123123'));

					// If password is invalid
					assert.isNotOk(users[0].validPassword('omfg123'));

					done();
				}
			);
		});

		it('Checks user permissions', done => {
			var scopes = ['read:post', 'write:post'];

			// Create Editor role
			Role.create({
				id: 'editor',
				grants: scopes
			}, (err, role) => {

				// Create an user
				User.create(require('./user.factory')(1),
					(err, users) => {
						if (err) return done(err);

						users[0].hasScopes(scopes, (err, authorized) => {
							// Check if user is granted to all scopes
							assert.isOk(authorized, true);

							done();
						});
					}
				);
			});
		});
	});

	describe('Endpoints', () => {
		it('Gets all user records', done => {
			User.create(require('./user.factory')(10),
				(err, users) => {
					if (err) return done(err);

					// GET request to /users
					chai.request(server)
					.get('/users')
					.end((err, res) => {
						if (err) return done(err);

						// Check if returned all the users
						assert.equal(res.body.length, 10);

						done();
					});
				}
			);
		});

		it('Finds user by ID', done => {
			User.create(require('./user.factory')(1),
				(err, users) => {
					if (err) return done(err);

					// GET request to /users
					chai.request(server)
					.get(`/users/${users[0].id}`)
					.end((err, res) => {
						if (err) return done(err);

						// Verify user names are equal
						assert.equal(res.body.full_name, users[0].full_name);

						done();
					});
				}
			);
		});

		it('Searches for users', done => {
			User.create(require('./user.factory')(10),
				(err, users) => {
					if (err) return done(err);

					// GET request to /users/search/:query
					chai.request(server)
					.get(`/users/search/${users[0].profile.email}`)
					.end((err, res) => {
						if (err) return done(err);

						// Verify if emails are the same
						assert.equal(res.body[0].profile.email, users[0].profile.email)
						done()
					});
				}
			);
		});

		it('Creates a new user', done => {
			chai.request(server)
			.post('/users')
			.send({
				profile: {
					email: 'giseudo@gmail.com',
					given_name: 'Giseudo',
					family_name: 'Oliveira'
				},
				password: '123123',
				roles: ['editor']
			})
			.end((err, res) => {
				if (err) return done(err);

				assert.equal(res.body.profile.email, 'giseudo@gmail.com');
				assert.equal(res.body.full_name, 'Giseudo Oliveira');
				assert.equal(res.body.accounts.local.username, 'giseudo@gmail.com');
				done();
			});
		});

		it('Updates the user object', (done) => {
			User.create(require('./user.factory')(2, {
				accounts: {
					local: {
						username: 'test',
						password: 'q1w2e3'
					}
				}
			}),
				(err, users) => {
					if (err) return done(err);

					chai.request(server)
					.patch(`/users/${users[0].id}`)
					.send({
						profile: {
							email: 'giseudo@gmail.com',
							given_name: 'Giseudo',
							family_name: 'Oliveira',
						},
						username: 'giseudo',
						password: '102030'
					})
					.end((err, res) => {
						if (err) return done(err);

						// Verify if user's email was changes
						assert.equal(res.body.profile.email, 'giseudo@gmail.com');
						assert.equal(res.body.full_name, 'Giseudo Oliveira');
						assert.equal(res.body.accounts.local.username, 'giseudo');
						// Get created user
						User.findOne({ _id: users[0].id }, '+accounts.local.password',
							(err, user) => {
								if (err) return done(err);

								// Check if password was changed
								assert.isOk(user.validPassword('102030'));

								// Check if profile data was not overwrited
								assert.isNotOk(user.profile.phone, undefined);
							});
					});

					// Check when empty password is passed
					chai.request(server)
					.patch(`/users/${users[1].id}`)
					.send({
						profile: {
							email: 'jonathan@gmail.com',
							given_name: 'Jonathan',
							family_name: 'Beltrão',
						},
						username: 'jonathan',
						password: ''
					})
					.end((err, res) => {
						// Get created user
						User.findOne({ _id: users[1].id }, '+accounts.local.password',
							(err, user) => {
								if (err) return done(err);

								// Check if password was not changed
								assert.isOk(user.validPassword('q1w2e3'));
								done();
							});
					});
				}
			);
		});

		it('Deletes user from database', (done) => {
			User.create(require('./user.factory')(1),
				(err, users) => {
					if(err) return done(err);

					chai.request(server)
					.delete(`/users/${users[0].id}`)
					.end((err, res) => {
						if (err) return done(err);

						User.findOne({ _id: users[0].id }, (err, user) => {
							if (err) return done(err);

							// Verify is user was deleted
							assert.isOk(!user);
							done();
						});
					})
				}
			);
		});
	});
});
