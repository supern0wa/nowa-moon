module.exports = (router, request) => {
	var Model = require('./user.model'),
		Controller = require('./user.controller')(request);

	router.get('/users', Controller.all);
	router.get('/users/:id', Controller.find);
	router.get('/users/search/:query', Controller.search);
	router.post('/users', Model.validations, Controller.store);
	router.patch('/users/:id', Controller.update);
	router.delete('/users/:id', Controller.destroy);
}
