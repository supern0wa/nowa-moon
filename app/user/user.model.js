let mongoose = require('mongoose'),
	bcrypt = require('bcryptjs');

const { check } = require('express-validator/check');

var Role = require('../role/role.model');

var UserSchema = mongoose.Schema({
	profile: {
		email: {
			type: String,
			unique: true
		},
		email_verified: Boolean,
		given_name: String,
		family_name: String,
		picture: String,
		phones: [String],
		website: String,
		gender: String,
		birthdate: Date,
		locale: String
	},
	accounts: {
		local: {
			username: String,
			password: {
				type: String,
				select: false
			}
		}
	},
	roles: [String],
	blocked: Boolean,
	login_count: Number,
	last_login: Date,
	created_at: Date,
	updated_at: Date
}, {
	toJSON: {
		getters: true,
		virtuals: true
	},
	toObject: {
		getters: true,
		virtuals: true
	}
});

UserSchema.virtual('full_name').get(function() {
	return `${this.profile.given_name} ${this.profile.family_name}`;
});

UserSchema.pre('save', function(next) {
	// Set created at and updated at fields
	this.updated_at = new Date();

	// If an username wasn't found use email
	if (!this.accounts.local.username)
		this.accounts.local.username = this.profile.email

	// Only hash the password if it has been modified
	if (!this.isModified('accounts.local.password')) return next();

	// Hash the password
	this.accounts.local.password = UserSchema.statics
		.generateHash(this.accounts.local.password);

	next();
});

// Generating a hash
UserSchema.statics.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// Search UserSchema
UserSchema.statics.search = function(query, callback) {
	return this.find({
		$or: [
			{ 'profile.name': new RegExp(query, 'i') },
			{ 'profile.email': new RegExp(query, 'i') }
		]
	}).exec(callback);
};

// Check if password is valid
UserSchema.methods.validPassword = function(password) {
	var valid = false;

	if (this.accounts.local)
		valid = bcrypt.compareSync(password, this.accounts.local.password);

	return valid;
};

UserSchema.methods.getScopes = function(done) {
	return Role.find({
		id: {
			$in: this.roles
		}
	}, (err, roles) => {
		if (err) return done(err);

		var scopes = [];

		// Push role grants to scope array
		roles.forEach(role => scopes.push(...role.grants));

		return done(null, scopes);
	});
};

UserSchema.methods.hasScopes = function(scopes, done) {
	return this.getScopes((err, available) => {
		if (err) return done(err);

		// Verify if all request scopes are available to user
		scopes.forEach(scope => {
			if (!available.includes(scope))
				return done(null, false);
		});

		return done(null, true);
	});
};

UserSchema.statics.validations = [
	check('profile.given_name').exists().withMessage('First name is required.'),
	check('profile.family_name').exists().withMessage('Last name is required.'),
	check('profile.email').exists().withMessage('A valid email is required.'),
	check('roles').exists().withMessage('At least one role is required.'),
	check('password').exists().withMessage('The password is required.')
];

// Create the model for UserSchemas and expose it to out app
module.exports = mongoose.model('User', UserSchema);
