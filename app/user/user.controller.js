const { validationResult } = require('express-validator/check'),
	Boom = require('boom'),
	User = require('./user.model');

module.exports = (request) => ({
	all(req, res, next) {
		User.find({}, (err, users) => {
			if (err) return next(err);

			res.json(users);
		});
	},

	find(req, res, next) {
		User.findOne({ _id: req.params.id }, (err, user) => {
			if (err) return next(err);

			res.json(user);
		});
	},

	search(req, res, next) {
		User.search(req.params.query, (err, users) => {
			if (err) return next(err);

			res.json(users);
		});
	},

	store(req, res, next) {
		const err = validationResult(req);

		if (!err.isEmpty())
			return next(Boom.badRequest(err.array()[0].msg));

		User.create({
			'profile': req.body.profile,
			'accounts.local.username': req.body.username,
			'accounts.local.password': req.body.password,
			'roles': req.body.roles,
			'created_at': new Date()
		}, (err, user) => {
			if (err) return next(err);

			res.json(user);
		})
	},

	update(req, res, next) {
		const err = validationResult(req);

		if (!err.isEmpty())
			return next(Boom.badRequest(err.array()[0].msg));

		User.findOne({ _id: req.params.id }, (err, user) => {
			if (err) return next(err);

			// Merge profile data
			user.profile = Object.assign(user.profile, req.body.profile);

			// Set local accounts
			user.accounts.local.username = req.body.username;

			// Set password only if it's valid
			if (req.body.password && req.body.password.length > 0)
				user.accounts.local.password = req.body.password;

			// Set roles
			user.accounts.roles = req.body.roles;

			// Save and return
			user.save();
			res.json(user);
		});
	},

	destroy(req, res, next) {
		User.remove({ _id: req.params.id }, (err, user) => {
			if (err) return next(err);

			res.json(user)
		});
	},
})
