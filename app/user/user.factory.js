var faker = require('faker'),
	User = require('./user.model');

module.exports = (amount, data) => {
	var batch = [];

	for (var i = 0; i < (amount || 1); i++) {
		var first_name = faker.name.firstName(),
			last_name = faker.name.lastName();

		batch.push(Object.assign({
			profile: {
				name: `${first_name} ${last_name}`,
				email: faker.internet.email(),
				email_verified: false,
				given_name: first_name,
				family_name: last_name,
				picture: faker.image.avatar(),
				phones: [
					faker.phone.phoneNumber()
				],
				website: faker.internet.url(),
				gender: 'male',
				birthdate: faker.date.past(),
				locale: faker.random.locale()
			},
			roles: [
				'member', 'editor', 'operator', 'admin'
			],
			accounts: {
				local: {
					username: faker.internet.userName(),
					password: faker.internet.password()
				}
			},
			blocked: false,
			login_count: 0,
			last_login: null,
			created_at: new Date(),
			updated_at: new Date()
		}, data || {}));
	}

	return batch;
};
