var server = require('../../server'),
	request = require('request'),
	sinon = require('sinon'),
	chai = require('chai'),
	assert = chai.assert;

var User = require('../user/user.model');

describe('Local Authentication', () => {
	var authenticated_id = null;

	before(done => {
		chai.request(server)
		.post('/roles')
		.send({
			id: 'member',
			grants: ['write:post', 'read:post'],
		})
		.end(done);
	});

	after(done => {
		User.remove({});
		chai.request(server)
		.delete('/roles/member')
		.end(done);
	});

	describe('Registration', () => {
		it('Registers with wrong input', done => {
			chai.request(server)
			.post('/auth/register')
			.send({
				username: 'gis',
				password: '1231',
				email: 'giseudo',
				first_name: 'Giseudo',
			})
			.end((err, res) => {
				var errors = res.body.map(error => error['param']);

				// Errors are expected
				assert.isNotOk(!err);

				// Password is weak
				assert.oneOf('password', errors);

				// Email is invalid
				assert.oneOf('email', errors);

				// Last name wasn't given
				assert.oneOf('last_name', errors);
				done();
			})
		});

		it('Registers with valid input', done => {
			chai.request(server)
			.post('/auth/register')
			.send({
				username: 'giseudo',
				password: 'q1w2e3',
				email: 'giseudo@gmail.com',
				first_name: 'Giseudo',
				last_name: 'Oliveira'
			})
			.end((err, res) => {
				if (err) return done(err);

				// Check if user was registered
				assert.isOk(res.body.user_id);
				done();
			})
		});

		it('Registers with existing username', done => {
			chai.request(server)
			.post('/auth/register')
			.send({
				username: 'giseudo',
				password: 'q1w2e3',
				email: 'giseudo@gmail.com',
				first_name: 'Giseudo',
				last_name: 'Oliveira'
			})
			.end((err, res) => {
				// Errors are expected
				assert.isNotOk(!err);

				// Check for bad request error
				assert.equal(res.status, 400);
				done();
			})
		});
	})

	describe('Authentication', () => {
		before(done => {
			// Stub authorization request
			sinon.stub(request, 'post')
				.yields(null, null, {
					access_token: '123123',
					refresh_token: '123123',
					expires_in: new Date().getTime() / 1000
				});
			done();
		});

		after(done => {
			request.post.restore();
			done();
		});

		it('Enters invalid credentials', done => {
			chai.request(server)
			.post('/auth/login')
			.send({
				username: 'giseudo@gmail',
				password: 'q1w2'
			})
			.end((err, res) => {
				var errors = res.body.map(error => error['param']);

				// Errors are expected
				assert.isNotOk(!err);

				// Email is invalid
				assert.oneOf('password', errors);
				done();
			})
		});

		it('Enters valid credentials', done => {
			chai.request(server)
			.post('/auth/login')
			.send({
				username: 'giseudo@gmail.com',
				password: 'q1w2e3',
			})
			.end((err, res) => {
				if (err) return done(err);

				// Check if user id was returned
				assert.isOk(res.body.user_id);

				// Set authenticated id
				authenticated_id = res.body.user_id;

				done();
			})
		});

		it('Gets user data without authenticated id header', done => {
			chai.request(server)
			.get('/auth/userinfo')
			.end((err, res) => {
				// Errors are expected
				assert.isNotOk(!err);

				done();
			})
		});

		it('Gets user data', done => {
			chai.request(server)
			.get('/auth/userinfo')
			.set('x-authenticated-userid', authenticated_id)
			.end((err, res) => {
				if (err) return done(err);

				assert.equal(res.body.profile.email, 'giseudo@gmail.com');

				done();
			})
		});
	})
});
