let Boom = require('boom'),
	FacebookStrategy = require('passport-facebook').Strategy,
	User = require('../user/user.model');

let config = require('../../config/auth');

module.exports = (passport) => {
	// Facebook strategy implementation
	passport.use(new FacebookStrategy(config.facebook,
		(accessToken, refreshToken, profile, done) => {
			process.nextTick(() => {
				User.findOne({
					'facebook.id': profile.id
				}, (err, user) => {
					if (err)
						return done(err);

					if (user) {
						return done(null, user);
					} else {
						var newUser = new User();
						
						newUser.facebook.id = profile.id;
						newUser.facebook.token = accessToken;
						newUser.facebook.name = `${profile.name.givenName} ${profile.name.familyName}`;
						newUser.facebook.email = profile.emails[0].value;

						newUser.save((err) => {
							if (err)
								throw err;

							return done(null, newUser);
						});
					}
				});
			})
		})
	);

	return {
		connect(req, res, next) {
			//
		},

		callback(req, res, next) {
			//
		}
	}
};
