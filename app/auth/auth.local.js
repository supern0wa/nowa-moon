const { validationResult } = require('express-validator/check'),
	Boom = require('boom'),
	User = require('../user/user.model'),
	request = require('request');

module.exports = (baseRequest) => ({
	login(req, res, next) {
		try {
			validationResult(req).throw();
		} catch (err) {
			return next(Boom.badRequest('Validation failed.', err.array()));
		}

		User.findOne({
			$or: [
				{ 'profile.email': req.body.username },
				{ 'accounts.local.username': req.body.username }
			]
		}, '+accounts.local.password', (err, user) => {
			if (err)
				return next(Boom.boomify(err));

			// There's no user with this email
			if (!user)
				return next(Boom.unauthorized(`User ${req.body.username} not found.`));

			// The password doesn't match
			if (!user.validPassword(req.body.password))
				return next(Boom.unauthorized('Oops! Wrong password.'));

			// Return user
			res.json({
				user_id: user._id,
			});
		});
	},

	register(req, res, next) {
		try {
			validationResult(req).throw();
		} catch (err) {
			return next(Boom.badRequest('Validation failed.', err.array()));
		}

		User.findOne({
			$or: [
				{ 'profile.email': req.body.email },
				{ 'accounts.local.username': req.body.username || req.body.email }
			]
		}, (err, user) => {
			if (err)
				return next(Boom.boomify(err));

			// Check if user already exists
			if (user)
				return next(Boom.badRequest('That email is already in use.'));

			// Create user
			User.create({
				profile: {
					given_name: req.body.first_name,
					family_name: req.body.last_name,
					email: req.body.email
				},
				roles: ['member'],
				accounts: {
					local: {
						username: req.body.username,
						password: req.body.password
					}
				}
			}, (err, user) => {
				if (err)
					return next(Boom.boomify(err));

				// Return user
				res.json({
					user_id: user._id,
				});
			});
		});
	},

	info(req, res, next) {
		var user_id = req.headers['x-authenticated-userid'];

		if (!user_id)
			return next(Boom.badRequest('No authenticated user header'));

		User.findOne({
			_id: user_id
		}, (err, user) => {
			if (err)
				return next(Boom.boomify(err));

			res.json({
				profile: user.profile,
				roles: user.roles,
				created_at: user.created_at,
				updated_at: user.updated_at
			});
		})
	}
});
