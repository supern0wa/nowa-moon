var { check } = require('express-validator/check');

module.exports = (router, passport, request) => {
	var local = require('./auth.local')(request),
		twitter = require('./auth.twitter')(passport, request),
		facebook = require('./auth.facebook')(passport, request);

	// Sign in
	router.post('/auth/login', [
		check('password', 'passwords must be at least 5 chars long and contain one number')
			.exists()
			.isLength({ min: 6 })
			.matches(/\d/),
		check('username', 'username is required').exists(),
	], local.login);

	// Register
	router.post('/auth/register', [
		check('password', 'passwords must be at least 5 chars long and contain one number')
			.exists()
			.isLength({ min: 6 })
			.matches(/\d/),
		check('email', 'email should be valid').exists().isEmail(),
		check('first_name', 'first name is required').exists(),
		check('last_name', 'last name is required').exists()
	], local.register);

	// Userinfo
	router.get('/auth/userinfo', local.info);

	// TODO: Reset Password
	// router.post('/auth/password', local.password);

	// TODO: Twitter
	// router.get('/auth/twitter', passport.authenticate('twitter'));
	// router.get('/auth/twitter/connect', twitter.connect);
	// router.get('/auth/twitter/callback', twitter.callback);

	// TODO: Facebook
	// router.get('/auth/facebook', passport.authenticate('facebook'));
	// router.get('/auth/facebook/connect', facebook.connect);
	// router.get('/auth/facebook/callback', facebook.callback);

	// TODO: Google
	// router.get('/auth/google', passport.authenticate('google'));
	// router.get('/auth/google/connect', google.connect);
	// router.get('/auth/google/callback', google.callback);
}
