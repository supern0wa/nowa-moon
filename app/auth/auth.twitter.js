let Boom = require('boom'),
	TwitterStrategy = require('passport-twitter').Strategy,
	User = require('../user/user.model');

let config = require('../../config/auth');

module.exports = (passport) => {
	// Twitter strategy implementation
	passport.use(new TwitterStrategy(config.twitter,
		(accessToken, accessTokenSecret, profile, done) => {
			process.nextTick(() => {
				User.findOne({
					'twitter.id': profile.id
				}, (err, user) => {
					if (err)
						return done(err);

					if (user) {
						return done(null, user);
					} else {
						let newUser = new User();
						
						newUser.twitter.id = profile.id;
						newUser.twitter.accessToken = accessToken;
						newUser.twitter.username = profile.username;
						newUser.twitter.displayName = profile.displayName;

						newUser.save(err => {
							if (err)
								throw err;

							return done(null, newUser);
						});
					}
				});
			})
		})
	);

	return {
		connect(req, res, next) {
			//
		},

		callback(req, res, next) {
			//
		}
	}
};
