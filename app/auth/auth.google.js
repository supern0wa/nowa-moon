let Boom = require('boom'),
	GoogleStrategy = require('google-twitter').Strategy,
	User = require('../models/user');

let auth = require('../../config/auth');

module.exports = (passport, config) => {
	// Google strategy implementation
	passport.use(new GoogleStrategy(config.google,
		() => {
			return false;
		})
	);

	return {
		connect(req, res, next) {
			//
		},

		callback(req, res, next) {
			//
		}
	}
};
