require('dotenv').config();

var chai = require('chai');
var should = chai.should();
var server = require('../server');
var request = require('request');

chai.use(require('chai-http'));

var User = require('../app/models/user');

describe('Login', () => {
	var user = new User();

	// Populate user object
	user.email = 'giseudo@gmail.com';
	user.accounts = {
		local: {
			email: 'giseudo@gmail.com',
			password: user.generateHash('123123')
		},
		facebook: {
			id: '118402568886448',
			token: '',
			email: 'open_exlxlbw_user@tfbnw.net',
			name: 'Open Graph Test User'
		},
		twitter: {
			id: '',
			token: '',
			displayName: '',
			username: ''
		},
		google: {
			id: '',
			token: '',
			email: '',
			name: ''
		}
	};

	describe('Authentication', () => {
		/*describe('Through Local strategy', () => {
			it('Signs in using local strategy.', client => {
			});
		});*/

		describe('Through Facebook strategy', () => {
			var fb_user = null

			before((client, done) => {
				client.init();

				// Remove all users from DB
				User.remove({}, err => {
					if (err)
						throw err;

					// Get FB Access Token
					request({
						method: 'GET',
						baseUrl: 'https://graph.facebook.com',
						uri: '/oauth/access_token',
						qs: {
							client_id: process.env.FACEBOOK_APP_ID,
							client_secret: process.env.FACEBOOK_APP_SECRET,
							grant_type: 'client_credentials'
						},
						json: true
					}, (err, res, body) => {
						if (err)
							throw err;

						// Set Access Token to a variable
						var access_token = body.access_token;

						// Create test user
						request({
							method: 'POST',
							baseUrl: 'https://graph.facebook.com',
							uri: `/v2.10/${process.env.FACEBOOK_APP_ID}/accounts/test-users`,
							body: {
								access_token,
								installed: false,
								permissions: '',
								name: 'Nowa Auth Test User'
							},
							json: true
						}, (err, res, body) => {
							if (err)
								throw err;

							fb_user = body;
							done();
						});
					});
				});
			});

			after((client, done) => {
				client.end(() => {
					done();
				});
			});

			beforeEach((client, done) => {
				done();
			});

			afterEach((client, done) => {
				done();
			})

			it('Signs in using facebook strategy.', client => {
				chai.request(server)
				.get('/auth/facebook')
				.end((err, res) => {
					var url = res.redirects[0];

					res.should.redirectTo(url);

					client.url(url)
					.expect.element('body').to.be.present.before(2000);
				});
			});

			it('Reject permission', () => {
			
			})

			it('Grants permission', () => {
			
			})
		});
	})

	describe('Authorizing', () => {
		var access_token = '',
			id_token = '';

		it('Asks Access Token', () => {
		
		})

		it('Generates an ID Token', () => {
		
		})

		it('Should return an Access + ID Token', () => {
		
		});
	})
});


