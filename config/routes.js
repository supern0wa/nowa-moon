module.exports = (router, passport, request) => {
	// Auth
	require('../app/auth/auth.routes')(router, passport, request);

	// User
	require('../app/user/user.routes')(router, request);

	// Role
	require('../app/role/role.routes')(router, request);

	return router;
};
