module.exports = {
	url: `${process.env.DB_HOST}/${process.env.NODE_ENV != 'test' ? process.env.DB_DATABASE : 'test'}`
};
