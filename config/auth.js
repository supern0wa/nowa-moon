module.exports = {
	local: {
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	facebook: {
		clientID: process.env.FACEBOOK_APP_ID,
		clientSecret: process.env.FACEBOOK_APP_SECRET,
		callbackURL: process.env.FACEBOOK_APP_CALLBACK,
		profileFields: ['email', 'first_name', 'last_name']
	},
	twitter: {
		consumerKey: process.env.TWITTER_APP_ID,
		consumerSecret: process.env.TWITTER_APP_SECRET,
		callbackURL: process.env.TWITTER_APP_CALLBACK
	},
	google: {
		clientID: process.env.GOOGLE_APP_ID,
		clientSecret: process.env.GOOGLE_APP_SECRET,
		callbackURL: process.env.GOOGLE_APP_CALLBACK
	}
};
