var express = require('express'),
	request = require('request'),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	expressValidator = require('express-validator'),
	passport = require('passport'),
	mongoose = require('mongoose');

let app = express(),
	router = express.Router();
	port = process.env.APP_PORT || 3000,
	database = require('./config/database');

// Set proxy to request
request.defaults().defaults({
	proxy: process.env.PROXY_URL || 'https://kong:8443',
	tunnel: false
});

// Connect to database
mongoose.connect(database.url, {
	useMongoClient: true
});

app.use(passport.initialize());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator({}));
app.use(session({
	secret: process.env.API_KEY,
	resave: true,
	saveUninitialized: true
}));

// Application routes
app.use(require('./config/routes')(router, passport, request));

// Error middlware
app.use((err, req, res, next) => {
	if (err.output) {
		let payload = err.output.payload;

		if (payload.statusCode >= 500)
			console.log(err.stack);

		res.statusMessage = payload.error;
		res.status(payload.statusCode);
		res.json(err.data);
		res.send(payload.message);
	} else {
		console.log(err.stack);
	}
});

// Listen to default port
app.listen(port, (err) => {
	if (err)
		console.error(`Error: ${err}`);

	console.log(`Server started ${port}`);
});

module.exports = app;
